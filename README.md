<img src="./img/Presentation.jpg">

# COVID 19 Timeline

A flutter app to track the timeline of COVID-19.

It uses the dataset from [pomber/covid19](https://github.com/pomber/covid19)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
