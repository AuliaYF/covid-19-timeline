import 'package:intl/intl.dart';

class Helpers {
  static String formatCurrency(
    double amount, {
    int decimalDigits = 0,
    bool symbol = false,
    String symbolText = 'Rp. ',
  }) {
    return NumberFormat.currency(
      symbol: symbol ? symbolText : '',
      decimalDigits: decimalDigits,
    ).format(amount);
  }
}
