part of 'dashboard_bloc.dart';

abstract class DashboardState extends Equatable {
  const DashboardState();
}

class DashboardInitial extends DashboardState {
  @override
  List<Object> get props => [];
}

class DashboardLoading extends DashboardState {
  @override
  List<Object> get props => null;
}

class DashboardFailed extends DashboardState {
  final String errorMessage;

  DashboardFailed({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class DashboardLoaded extends DashboardState {
  final SummaryModel confirmedSummary;
  final SummaryModel deathSummary;
  final SummaryModel recoveredSummary;
  final DateTime updatedAt;
  final List<CountryModel> countries;
  final List<CountryModel> filtered;
  final DateTime minDate;
  final DateTime maxDate;
  final DateTime currentDate;
  final List<TimelineModel> dailyTimelines;

  DashboardLoaded({
    this.confirmedSummary,
    this.deathSummary,
    this.recoveredSummary,
    this.updatedAt,
    this.countries,
    this.filtered,
    this.minDate,
    this.maxDate,
    this.currentDate,
    this.dailyTimelines,
  });

  @override
  List<Object> get props => [
        confirmedSummary,
        deathSummary,
        recoveredSummary,
        updatedAt,
        countries,
        filtered,
        minDate,
        maxDate,
        currentDate,
        dailyTimelines,
      ];
}
