part of 'dashboard_bloc.dart';

abstract class DashboardEvent extends Equatable {
  const DashboardEvent();
}

class EventDoGetDashboard extends DashboardEvent {
  final bool refresh;
  final SortOptions sort;

  EventDoGetDashboard({this.refresh = false, this.sort});

  @override
  List<Object> get props => [refresh, sort];
}

class EventDoFilterData extends DashboardEvent {
  final DateTime date;
  final SortOptions sort;

  EventDoFilterData({this.date, this.sort});

  @override
  List<Object> get props => [date, sort];
}
