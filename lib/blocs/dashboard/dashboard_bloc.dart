import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid19/models/index.dart';
import 'package:covid19/repos/index.dart';
import 'package:equatable/equatable.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

enum SortOptions {
  countryAsc,
  countryDesc,
  confirmedAsc,
  confirmedDesc,
  deathsAsc,
  deathsDesc,
  recoveredAsc,
  recoveredDesc,
}

final Map<SortOptions, String> SortOptionsLabels = {
  SortOptions.countryAsc: 'COUNTRY ASC',
  SortOptions.countryDesc: 'COUNTRY DESC',
  SortOptions.confirmedAsc: 'CONFIRMED ASC',
  SortOptions.confirmedDesc: 'CONFIRMED DESC',
  SortOptions.deathsAsc: 'DEATHS ASC',
  SortOptions.deathsDesc: 'DEATHS DESC',
  SortOptions.recoveredAsc: 'RECOVERED ASC',
  SortOptions.recoveredDesc: 'RECOVERED DESC',
};

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  final GeneralRepository _repo;

  DashboardBloc({GeneralRepository repo}) : this._repo = repo;

  @override
  DashboardState get initialState => DashboardInitial();

  @override
  Stream<DashboardState> mapEventToState(
    DashboardEvent event,
  ) async* {
    if (event is EventDoGetDashboard || event is EventDoFilterData) {
      if (event is EventDoGetDashboard && event.refresh) {
        yield DashboardInitial();
      }

      SortOptions sort = SortOptions.countryAsc;

      if (event is EventDoGetDashboard) {
        sort = event.sort;
      } else if (event is EventDoFilterData) {
        sort = event.sort;
      }

      try {
        List<CountryModel> countries = [];
        List<CountryModel> filtered = [];

        if (event is EventDoGetDashboard) {
          countries = await this._repo.requestData();
        } else if (event is EventDoFilterData && state is DashboardLoaded) {
          countries = (state as DashboardLoaded).countries;
        }

        SummaryDirection confirmedDirection = SummaryDirection.flat;
        SummaryDirection deathDirection = SummaryDirection.flat;
        SummaryDirection recoveredDirection = SummaryDirection.flat;

        int confirmedTotal = 0;
        int deathTotal = 0;
        int recoveredTotal = 0;

        int confirmedToday = 0;
        int deathToday = 0;
        int recoveredToday = 0;

        DateTime updatedAt;

        Map<DateTime, int> dailyConfirmed = {};
        Map<DateTime, int> dailyDeaths = {};
        Map<DateTime, int> dailyRecovered = {};

        DateTime minDate;
        DateTime maxDate;

        countries.forEach((country) {
          if (country.timeline != null) {
            int prevConfirmed = 0;
            int prevDeaths = 0;
            int prevRecovered = 0;

            country.timeline.sort((a, b) => a.date.compareTo(b.date));

            List<TimelineModel> timeline = country.timeline;

            SummaryDirection countryConfirmedDir = SummaryDirection.flat;
            int countryConfirmedToday = 0;
            double countryConfirmedDiff = 0.0;

            SummaryDirection countryDeathsDir = SummaryDirection.flat;
            int countryDeathsToday = 0;
            double countryDeathsDiff = 0.0;

            SummaryDirection countryRecoveredDir = SummaryDirection.flat;
            int countryRecoveredToday = 0;
            double countryRecoveredDiff = 0.0;

            timeline.forEach((data) {
              if (minDate == null ||
                  (minDate != null && data.date.isBefore(minDate))) {
                minDate = data.date;
              }

              if (maxDate == null ||
                  (maxDate != null && data.date.isAfter(maxDate))) {
                maxDate = data.date;
              }

              if (updatedAt == null ||
                  (updatedAt != null && data.date.isAfter(updatedAt))) {
                updatedAt = data.date;
              }
            });

            if (event is EventDoFilterData) {
              timeline = timeline.takeWhile((data) {
                return data.date.isBefore(event.date) ||
                    data.date == event.date;
              }).toList();
            }

            timeline.forEach((timeline) {
              if (!dailyConfirmed.containsKey(timeline.date)) {
                dailyConfirmed[timeline.date] = 0;
              }

              if (!dailyDeaths.containsKey(timeline.date)) {
                dailyDeaths[timeline.date] = 0;
              }

              if (!dailyRecovered.containsKey(timeline.date)) {
                dailyRecovered[timeline.date] = 0;
              }

              dailyConfirmed[timeline.date] += timeline.confirmed;
              dailyDeaths[timeline.date] += timeline.deaths;
              dailyRecovered[timeline.date] += timeline.recovered;

              if (DateTime.now().difference(timeline.date).inDays == 0) {
                confirmedToday += timeline.confirmed;
                deathToday += timeline.deaths;
                recoveredToday += timeline.recovered;

                countryConfirmedToday = timeline.confirmed;
                countryDeathsToday = timeline.deaths;
                countryRecoveredToday = timeline.recovered;
              }

              if (timeline.confirmed > prevConfirmed) {
                countryConfirmedDir = SummaryDirection.up;
              } else if (timeline.confirmed < prevConfirmed) {
                countryConfirmedDir = SummaryDirection.down;
              } else {
                countryConfirmedDir = SummaryDirection.flat;
              }

              final confirmedDiff = (timeline.confirmed - prevConfirmed).abs();
              countryConfirmedDiff = (confirmedDiff / timeline.confirmed) * 100;

              if (timeline.deaths > prevDeaths) {
                countryDeathsDir = SummaryDirection.up;
              } else if (timeline.deaths < prevDeaths) {
                countryDeathsDir = SummaryDirection.down;
              } else {
                countryDeathsDir = SummaryDirection.flat;
              }

              final deathsDiff = (timeline.deaths - prevDeaths).abs();
              countryDeathsDiff = (deathsDiff / timeline.deaths) * 100;

              if (timeline.recovered > prevRecovered) {
                countryRecoveredDir = SummaryDirection.up;
              } else if (timeline.recovered < prevRecovered) {
                countryRecoveredDir = SummaryDirection.down;
              } else {
                countryRecoveredDir = SummaryDirection.flat;
              }

              final recoveredDiff = (timeline.recovered - prevRecovered).abs();
              countryRecoveredDiff = (recoveredDiff / timeline.recovered) * 100;

              prevConfirmed = timeline.confirmed;
              prevDeaths = timeline.deaths;
              prevRecovered = timeline.recovered;
            });

            country.confirmed = SummaryModel(
              today: countryConfirmedToday,
              diff: countryConfirmedDiff,
              direction: countryConfirmedDir,
              total: prevConfirmed,
            );
            country.deaths = SummaryModel(
              today: countryDeathsToday,
              diff: countryDeathsDiff,
              direction: countryDeathsDir,
              total: prevDeaths,
            );
            country.recovered = SummaryModel(
              today: countryRecoveredToday,
              diff: countryRecoveredDiff,
              direction: countryRecoveredDir,
              total: prevRecovered,
            );

            final int currentIndex = countries.indexOf(country);
            countries[currentIndex] = country;

            filtered.add(country);

            confirmedTotal += prevConfirmed;
            deathTotal += prevDeaths;
            recoveredTotal += prevRecovered;
          }
        });

        double diffConfirmed = 0;
        int prevConfirmed = 0;
        dailyConfirmed.values.forEach((value) {
          if (value > prevConfirmed) {
            confirmedDirection = SummaryDirection.up;
          } else if (value < prevConfirmed) {
            confirmedDirection = SummaryDirection.down;
          } else {
            confirmedDirection = SummaryDirection.flat;
          }

          final diff = (value - prevConfirmed).abs();
          diffConfirmed = (diff / value) * 100;

          prevConfirmed = value;
        });

        double diffDeaths = 0;
        int prevDeaths = 0;
        dailyDeaths.values.forEach((value) {
          if (value > prevDeaths) {
            deathDirection = SummaryDirection.up;
          } else if (value < prevDeaths) {
            deathDirection = SummaryDirection.down;
          } else {
            deathDirection = SummaryDirection.flat;
          }

          final diff = (value - prevDeaths).abs();
          diffDeaths = (diff / value) * 100;

          prevDeaths = value;
        });

        double diffRecovered = 0;
        int prevRecovered = 0;
        dailyRecovered.values.forEach((value) {
          if (value > prevRecovered) {
            recoveredDirection = SummaryDirection.up;
          } else if (value < prevRecovered) {
            recoveredDirection = SummaryDirection.down;
          } else {
            recoveredDirection = SummaryDirection.flat;
          }

          final diff = (value - prevRecovered).abs();
          diffRecovered = (diff / value) * 100;

          prevRecovered = value;
        });

        switch (sort) {
          case SortOptions.countryDesc:
            filtered.sort((a, b) => b.name.compareTo(a.name));
            break;

          case SortOptions.confirmedAsc:
            filtered
                .sort((a, b) => a.confirmed.total.compareTo(b.confirmed.total));
            break;

          case SortOptions.confirmedDesc:
            filtered
                .sort((a, b) => b.confirmed.total.compareTo(a.confirmed.total));
            break;

          case SortOptions.deathsAsc:
            filtered.sort((a, b) => a.deaths.total.compareTo(b.deaths.total));
            break;

          case SortOptions.deathsDesc:
            filtered.sort((a, b) => b.deaths.total.compareTo(a.deaths.total));
            break;

          case SortOptions.recoveredAsc:
            filtered
                .sort((a, b) => a.recovered.total.compareTo(b.recovered.total));
            break;

          case SortOptions.recoveredDesc:
            filtered
                .sort((a, b) => b.recovered.total.compareTo(a.recovered.total));
            break;

          default:
            filtered.sort((a, b) => a.name.compareTo(b.name));
            break;
        }

        List<TimelineModel> dailyTimelines = dailyConfirmed.keys.map((date) {
          return TimelineModel(
            date: date,
            confirmed: dailyConfirmed[date],
            deaths: dailyDeaths[date],
            recovered: dailyRecovered[date],
          );
        }).toList();

        yield DashboardLoaded(
          confirmedSummary: SummaryModel(
            direction: confirmedDirection,
            diff: diffConfirmed,
            total: confirmedTotal,
            today: confirmedToday,
          ),
          deathSummary: SummaryModel(
            direction: deathDirection,
            diff: diffDeaths,
            total: deathTotal,
            today: deathToday,
          ),
          recoveredSummary: SummaryModel(
            direction: recoveredDirection,
            diff: diffRecovered,
            total: recoveredTotal,
            today: recoveredToday,
          ),
          updatedAt: updatedAt,
          countries: countries,
          filtered: filtered,
          minDate: minDate,
          maxDate: maxDate,
          currentDate: event is EventDoFilterData ? event.date : maxDate,
          dailyTimelines: dailyTimelines,
        );
      } catch (e) {
        yield DashboardFailed(errorMessage: e.toString());
      }
    }
  }
}
