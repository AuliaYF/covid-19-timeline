import 'dart:async';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:covid19/blocs/dashboard/dashboard_bloc.dart';
import 'package:covid19/extensions.dart';
import 'package:covid19/models/index.dart';
import 'package:covid19/pages/index.dart';
import 'package:covid19/routes.dart';
import 'package:covid19/widgets/index.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Completer _completer = Completer();
  DateTime _currentDate;

  void _loadData({bool refresh = false}) {
    final SortOptionProvider sortProvider = Provider.of<SortOptionProvider>(
      context,
      listen: false,
    );
    final DashboardBloc bloc = BlocProvider.of<DashboardBloc>(context);

    bloc.add(EventDoGetDashboard(refresh: refresh, sort: sortProvider.sort));
  }

  void _sort(SortOptions sort) {
    final SortOptionProvider sortProvider = Provider.of<SortOptionProvider>(
      context,
      listen: false,
    );

    sortProvider.sort = sort;

    final DashboardBloc bloc = BlocProvider.of<DashboardBloc>(context);
    bloc.add(
      EventDoFilterData(date: this._currentDate, sort: sort),
    );
  }

  Future<void> _onRefresh() {
    this._loadData();

    return this._completer.future;
  }

  @override
  void initState() {
    super.initState();

    this._loadData(refresh: true);
  }

  @override
  Widget build(BuildContext context) {
    final SortOptionProvider sortProvider =
        Provider.of<SortOptionProvider>(context);

    return BlocListener<DashboardBloc, DashboardState>(
      listener: (context, state) {
        if (state is DashboardLoaded) {
          if (this._currentDate != state.currentDate) {
            this._currentDate = state.currentDate;
          }
        }

        if (state is DashboardFailed) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.errorMessage),
            ),
          );
        }

        if (state is DashboardLoaded || state is DashboardFailed) {
          this._completer?.complete();

          this._completer = Completer();
        }
      },
      child: NestedScrollView(
        headerSliverBuilder: (context, _) {
          return [
            SliverAppBar(
              forceElevated: true,
              floating: true,
              pinned: true,
              title: BlocBuilder<DashboardBloc, DashboardState>(
                condition: (prevState, nextState) {
                  return !(nextState is DashboardLoading);
                },
                builder: (context, state) {
                  if (state is DashboardLoaded && state.currentDate != null) {
                    return AppBarTitle(
                      title: 'COVID-19 Timeline',
                      subtitle: 'As of ${state.currentDate.format(
                        [dd, ' ', MM, ' ', yyyy],
                      )}',
                    );
                  }

                  return AppBarTitle(
                    title: 'COVID-19 Timeline',
                  );
                },
              ),
              bottom: DateSlider(),
            ),
          ];
        },
        body: RefreshIndicator(
          onRefresh: this._onRefresh,
          child: Scrollbar(
            child: CustomScrollView(
              slivers: <Widget>[
                SliverPadding(
                  padding: const EdgeInsets.all(16.0),
                  sliver: SliverToBoxAdapter(
                    child: BlocBuilder<DashboardBloc, DashboardState>(
                      condition: (prevState, nextState) {
                        return !(nextState is DashboardLoading);
                      },
                      builder: (context, state) {
                        if (state is DashboardLoaded &&
                            state.confirmedSummary != null) {
                          return SummaryCard(
                            title: 'Confirmed',
                            badgeColor: Colors.blue,
                            badgeDirection: state.confirmedSummary.direction,
                            diff: state.confirmedSummary.diff,
                            total: state.confirmedSummary.total,
                            today: state.confirmedSummary.today,
                          );
                        }

                        return SummaryCard(
                          title: 'Confirmed',
                          badgeColor: Colors.blue,
                        );
                      },
                    ),
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.all(16.0).copyWith(top: 0.0),
                  sliver: SliverToBoxAdapter(
                    child: BlocBuilder<DashboardBloc, DashboardState>(
                      condition: (prevState, nextState) {
                        return !(nextState is DashboardLoading);
                      },
                      builder: (context, state) {
                        if (state is DashboardLoaded &&
                            state.deathSummary != null) {
                          return SummaryCard(
                            title: 'Deaths',
                            badgeColor: Colors.red,
                            badgeDirection: state.deathSummary.direction,
                            diff: state.deathSummary.diff,
                            total: state.deathSummary.total,
                            today: state.deathSummary.today,
                          );
                        }

                        return SummaryCard(
                          title: 'Deaths',
                          badgeColor: Colors.red,
                        );
                      },
                    ),
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.all(16.0).copyWith(top: 0.0),
                  sliver: SliverToBoxAdapter(
                    child: BlocBuilder<DashboardBloc, DashboardState>(
                      condition: (prevState, nextState) {
                        return !(nextState is DashboardLoading);
                      },
                      builder: (context, state) {
                        if (state is DashboardLoaded &&
                            state.recoveredSummary != null) {
                          return SummaryCard(
                            title: 'Recovered',
                            badgeColor: Colors.green,
                            badgeDirection: state.recoveredSummary.direction,
                            diff: state.recoveredSummary.diff,
                            total: state.recoveredSummary.total,
                            today: state.recoveredSummary.today,
                          );
                        }

                        return SummaryCard(
                          title: 'Recovered',
                          badgeColor: Colors.green,
                        );
                      },
                    ),
                  ),
                ),
                BlocBuilder<DashboardBloc, DashboardState>(
                  condition: (prevState, nextState) {
                    return !(nextState is DashboardLoading);
                  },
                  builder: (context, state) {
                    if (state is DashboardLoaded) {
                      return SliverPadding(
                        padding: const EdgeInsets.all(16.0).copyWith(top: 0.0),
                        sliver: SliverToBoxAdapter(
                          child: SummaryChart(
                            timeline: state.dailyTimelines,
                          ),
                        ),
                      );
                    }

                    return SliverToBoxAdapter();
                  },
                ),
                BlocBuilder<DashboardBloc, DashboardState>(
                  condition: (prevState, nextState) {
                    return !(nextState is DashboardLoading);
                  },
                  builder: (context, state) {
                    if (state is DashboardLoaded &&
                        (state.filtered ?? []).isNotEmpty) {
                      return SliverPadding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        sliver: SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, index) {
                              final country = state.filtered[index];

                              return Column(
                                children: <Widget>[
                                  if (index == 0) ...[
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: PopupMenuButton<SortOptions>(
                                        onSelected: this._sort,
                                        itemBuilder: (context) =>
                                            SortOptions.values.map((option) {
                                          return PopupMenuItem<SortOptions>(
                                            value: option,
                                            child:
                                                Text(SortOptionsLabels[option]),
                                          );
                                        }).toList(),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                            vertical: 8.0,
                                            horizontal: 16.0,
                                          ),
                                          child: Text(
                                            'SORT BY: ${SortOptionsLabels[sortProvider.sort]}',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 16.0),
                                  ],
                                  CountryListItem(
                                    onTap: () {
                                      Navigator.pushNamed(
                                        context,
                                        Routes.detail,
                                        arguments: {
                                          'country': country,
                                          'latestDate': state.currentDate,
                                        },
                                      );
                                    },
                                    name: country.name,
                                    confirmed: country.confirmed.total,
                                    deaths: country.deaths.total,
                                    recovered: country.recovered.total,
                                  ),
                                ],
                              );
                            },
                            childCount: state.filtered.length,
                          ),
                        ),
                      );
                    }

                    return SliverToBoxAdapter();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DateSlider extends StatefulWidget implements PreferredSizeWidget {
  @override
  _DateSliderState createState() => _DateSliderState();

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class _DateSliderState extends State<DateSlider> {
  double _value = 0.0;
  DateTime _minDate;
  DateTime _maxDate;
  DateTime _currentDate;
  Timer _timer;

  bool get _isTimerActive => _timer != null && _timer.isActive;

  void _onChanged(double value) {
    setState(() {
      this._value = value.round().toDouble();
      this._currentDate =
          this._minDate.add(Duration(days: this._value.toInt()));
    });
  }

  void _onChangeEnd(double value) {
    setState(() {
      this._value = value.round().toDouble();
      this._currentDate =
          this._minDate.add(Duration(days: this._value.toInt()));
    });

    final SortOptionProvider sortProvider = Provider.of<SortOptionProvider>(
      context,
      listen: false,
    );

    final DashboardBloc bloc = BlocProvider.of<DashboardBloc>(context);
    bloc.add(EventDoFilterData(
      date: this._currentDate,
      sort: sortProvider.sort,
    ));
  }

  void _toggle() {
    if (this._isTimerActive) {
      this._timer.cancel();
    } else {
      this._timer = Timer.periodic(Duration(seconds: 1), (_) {
        this._onChangeEnd(this._value + 1);

        if (this._currentDate == this._maxDate) {
          setState(() {
            this._timer.cancel();
          });
        }
      });
    }

    setState(() {});
  }

  @override
  void dispose() {
    this._timer?.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<DashboardBloc, DashboardState>(
      listener: (context, state) {
        if (state is DashboardLoaded) {
          this._minDate = state.minDate;
          this._maxDate = state.maxDate;

          if (this._currentDate != state.currentDate) {
            this._currentDate = state.currentDate;
            this._value =
                this._maxDate.difference(this._minDate).inDays.abs().toDouble();
          }
        }
      },
      child: Card(
        child: Container(
          height: kToolbarHeight - 8.0,
          child: BlocBuilder<DashboardBloc, DashboardState>(
            condition: (prevState, nextState) {
              return !(nextState is DashboardLoading);
            },
            builder: (context, state) {
              if (state is DashboardLoaded &&
                  state.minDate != null &&
                  state.maxDate != null) {
                return Row(
                  children: <Widget>[
                    Expanded(
                      child: Slider(
                        value: this._value,
                        min: 0,
                        max: state.maxDate
                            .difference(state.minDate)
                            .inDays
                            .abs()
                            .toDouble(),
                        divisions: state.maxDate
                            .difference(state.minDate)
                            .inDays
                            .abs(),
                        onChanged: this._onChanged,
                        onChangeEnd: this._onChangeEnd,
                        label: '${this._currentDate.format([
                          dd,
                          ' ',
                          MM,
                          ' ',
                          yyyy
                        ])}',
                      ),
                    ),
                    ClipOval(
                      child: Material(
                        color: Colors.transparent,
                        child: IconButton(
                          onPressed: this._currentDate != this._maxDate
                              ? this._toggle
                              : null,
                          icon: this._isTimerActive
                              ? const Icon(Icons.pause)
                              : const Icon(Icons.play_arrow),
                        ),
                      ),
                    ),
                    SizedBox(width: 16.0),
                  ],
                );
              }

              return Center(
                child: SizedBox(
                  height: 8.0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: LinearProgressIndicator(),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
