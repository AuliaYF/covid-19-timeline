import 'package:covid19/helpers.dart';
import 'package:covid19/models/index.dart';
import 'package:flutter/material.dart';

class CountryListItem extends StatelessWidget {
  const CountryListItem({
    Key key,
    this.onTap,
    @required this.name,
    this.confirmed = 0,
    this.deaths = 0,
    this.recovered = 0,
  }) : super(key: key);

  final Function onTap;
  final String name;
  final int confirmed;
  final int deaths;
  final int recovered;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(bottom: 16.0),
      child: InkWell(
        onTap: this.onTap,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[
              Text(
                '${CountryFlags[this.name]}',
                style: TextStyle(fontSize: 24.0),
              ),
              SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      this.name,
                      style: Theme.of(context).textTheme.subhead.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      '${Helpers.formatCurrency(this.confirmed.toDouble())} Cases',
                      style: Theme.of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.blue),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 16.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    '${Helpers.formatCurrency(this.recovered.toDouble())} Recovered',
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: Colors.green),
                  ),
                  SizedBox(height: 4.0),
                  Text(
                    '${Helpers.formatCurrency(this.deaths.toDouble())} Deaths',
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: Colors.red),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
