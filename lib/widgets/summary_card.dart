import 'package:badges/badges.dart';
import 'package:covid19/helpers.dart';
import 'package:covid19/models/index.dart';
import 'package:flutter/material.dart';

class SummaryCard extends StatelessWidget {
  const SummaryCard({
    Key key,
    @required this.title,
    @required this.badgeColor,
    this.badgeDirection = SummaryDirection.flat,
    this.diff = 0.0,
    this.total = 0,
    this.today = 0,
  }) : super(key: key);

  final String title;
  final Color badgeColor;
  final SummaryDirection badgeDirection;
  final double diff;
  final int total;
  final int today;

  @override
  Widget build(BuildContext context) {
    IconData badgeIcon = Icons.trending_flat;

    if (this.badgeDirection == SummaryDirection.up) {
      badgeIcon = Icons.trending_up;
    } else if (this.badgeDirection == SummaryDirection.down) {
      badgeIcon = Icons.trending_down;
    }

    return Card(
      margin: EdgeInsets.zero,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              this.title,
              style: Theme.of(context).textTheme.subhead.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            SizedBox(height: 16.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Badge(
                  toAnimate: false,
                  elevation: 0.0,
                  borderRadius: 32.0,
                  shape: BadgeShape.square,
                  badgeColor: this.badgeColor,
                  padding: const EdgeInsets.symmetric(
                    vertical: 4.0,
                    horizontal: 12.0,
                  ),
                  badgeContent: Row(
                    children: <Widget>[
                      Text(
                        '${Helpers.formatCurrency(this.diff, decimalDigits: 2)}%',
                        style: Theme.of(context)
                            .textTheme
                            .caption
                            .copyWith(color: Colors.white),
                      ),
                      SizedBox(width: 4.0),
                      Icon(
                        badgeIcon,
                        size: 14.0,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      '${Helpers.formatCurrency(this.total.toDouble())}',
                      style: Theme.of(context)
                          .textTheme
                          .title
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      '${Helpers.formatCurrency(this.today.toDouble())} today',
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
