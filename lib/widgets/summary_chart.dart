import 'package:covid19/helpers.dart';
import 'package:covid19/models/index.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class SummaryChart extends StatefulWidget {
  const SummaryChart({
    Key key,
    @required this.timeline,
  }) : super(key: key);

  final List<TimelineModel> timeline;

  @override
  _SummaryChartState createState() => _SummaryChartState();
}

class _SummaryChartState extends State<SummaryChart> {
  bool _loaded = false;
  Widget _chart;
  List<Widget> _measures;
  List<TimelineModel> _timelines;

  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;
    final measures = <Widget>[];

    if (selectedDatum.isNotEmpty) {
      selectedDatum.forEach((charts.SeriesDatum datumPair) {
        switch (datumPair.series.id) {
          case "deaths":
            measures.add(
              RichText(
                text: TextSpan(
                  text: "Deaths: ",
                  style: TextStyle(
                    color: Colors.red,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: Helpers.formatCurrency(
                        (datumPair.datum as TimelineModel).deaths.toDouble(),
                      ),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            );
            break;

          case "recovered":
            measures.add(
              RichText(
                text: TextSpan(
                  text: "Recovered: ",
                  style: TextStyle(
                    color: Colors.green,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: Helpers.formatCurrency(
                        (datumPair.datum as TimelineModel).recovered.toDouble(),
                      ),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            );
            break;

          default:
            measures.add(
              RichText(
                text: TextSpan(
                  text: "Confirmed: ",
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: Helpers.formatCurrency(
                        (datumPair.datum as TimelineModel).confirmed.toDouble(),
                      ),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            );
            break;
        }
      });
    }

    setState(() {
      this._measures = measures;
    });
  }

  @override
  void initState() {
    super.initState();

    this._timelines = this.widget.timeline;
  }

  @override
  Widget build(BuildContext context) {
    if (this._chart == null || this._timelines != this.widget.timeline) {
      this._timelines = this.widget.timeline;
      this._chart = _SummaryChart(
        listener: this._onSelectionChanged,
        timeline: this.widget.timeline,
      );
    }

    return Card(
      margin: EdgeInsets.zero,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            this._chart,
            if ((this._measures ?? []).isNotEmpty) ...[
              SizedBox(height: 16.0),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: this._measures.map((measure) {
                  return Padding(
                    padding: EdgeInsets.only(
                      top: this._measures.indexOf(measure) == 0 ? 0.0 : 8.0,
                    ),
                    child: measure,
                  );
                }).toList(),
              )
            ]
          ],
        ),
      ),
    );
  }
}

class _SummaryChart extends StatelessWidget {
  const _SummaryChart({
    Key key,
    @required this.timeline,
    this.listener,
  }) : super(key: key);

  final List<TimelineModel> timeline;
  final charts.SelectionModelListener listener;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 240.0,
      child: charts.TimeSeriesChart(
        [
          charts.Series<TimelineModel, DateTime>(
            id: 'confirmed',
            displayName: 'Confirmed',
            colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
            domainFn: (timeline, _) => timeline.date,
            measureFn: (timeline, _) => timeline.confirmed,
            data: this.timeline.map((timeline) {
              return TimelineModel(
                confirmed: timeline.confirmed,
                date: timeline.date,
              );
            }).toList(),
          ),
          charts.Series<TimelineModel, DateTime>(
            id: 'deaths',
            displayName: 'Deaths',
            colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
            domainFn: (timeline, _) => timeline.date,
            measureFn: (timeline, _) => timeline.deaths,
            data: this.timeline.map((timeline) {
              return TimelineModel(
                deaths: timeline.deaths,
                date: timeline.date,
              );
            }).toList(),
          ),
          charts.Series<TimelineModel, DateTime>(
            id: 'recovered',
            displayName: 'Recovered',
            colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
            domainFn: (timeline, _) => timeline.date,
            measureFn: (timeline, _) => timeline.recovered,
            data: this.timeline.map((timeline) {
              return TimelineModel(
                recovered: timeline.recovered,
                date: timeline.date,
              );
            }).toList(),
          ),
        ],
        animate: false,
        defaultRenderer: charts.LineRendererConfig(),
        dateTimeFactory: const charts.LocalDateTimeFactory(),
        selectionModels: [
          charts.SelectionModelConfig(
            type: charts.SelectionModelType.info,
            changedListener: this.listener,
          )
        ],
        behaviors: [charts.SeriesLegend(), charts.LinePointHighlighter()],
      ),
    );
  }
}
