import 'package:flutter/material.dart';

class AppBarTitle extends StatelessWidget {
  final String title;
  final String subtitle;
  final Color textColor;

  const AppBarTitle({
    Key key,
    this.title,
    this.subtitle,
    this.textColor = Colors.white,
  })  : assert(title != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          this.title,
          style: Theme.of(context)
              .primaryTextTheme
              .title
              .copyWith(color: this.textColor),
          textAlign: Theme.of(context).platform == TargetPlatform.android
              ? TextAlign.left
              : TextAlign.center,
        ),
        if ((this.subtitle ?? "").isNotEmpty) ...[
          SizedBox(height: 4.0),
          Text(
            this.subtitle,
            style: Theme.of(context)
                .textTheme
                .caption
                .copyWith(color: this.textColor),
            textAlign: Theme.of(context).platform == TargetPlatform.android
                ? TextAlign.left
                : TextAlign.center,
          ),
        ],
      ],
    );
  }
}
