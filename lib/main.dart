import 'package:covid19/repos/index.dart';
import 'package:covid19/routes.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:provider/provider.dart';

void main() {
  final Dio dio = Dio();

  if (!kReleaseMode) {
    dio.interceptors.add(
      PrettyDioLogger(
        maxWidth: 80,
        requestHeader: true,
        requestBody: true,
        responseBody: true,
      ),
    );
  }

  runApp(
    MultiProvider(
      providers: [
        Provider<GeneralRepository>(
          create: (context) => GeneralRepository(dio: dio),
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.dark,
      ),
      title: 'COVID-19 Timeline',
      routes: Routes.routes,
    );
  }
}
