import 'package:covid19/pages/index.dart';
import 'package:flutter/material.dart';

class Routes {
  static final String home = HomePage.route;
  static final String detail = DetailPage.route;

  static final Map<String, Widget Function(BuildContext)> routes = {
    Routes.home: (context) => HomePage(),
    Routes.detail: (context) => DetailPage(),
  };
}
