import 'package:covid19/constants.dart';
import 'package:covid19/models/index.dart';
import 'package:dio/dio.dart';

class GeneralRepository {
  final Dio _dio;

  GeneralRepository({Dio dio}) : this._dio = dio;

  Future<List<CountryModel>> requestData() async {
    final response = await this._dio.get(Constants.apiUrl);

    if (response.statusCode == 200) {
      if (response.data != null) {
        final data = response.data as Map<String, dynamic>;

        return (data).keys.map((countryName) {
          return CountryModel(
            name: countryName,
            timeline: (data[countryName] as List).map((timeline) {
              final List<String> dates =
                  (timeline['date'] as String).split('-');

              timeline['date'] =
                  "${dates[0].padLeft(4, '0')}-${dates[1].padLeft(2, '0')}-${dates[2].padLeft(2, '0')}";

              return TimelineModel.fromJson(timeline as Map<String, dynamic>);
            }).toList(),
          );
        }).toList();
      } else {
        return <CountryModel>[];
      }
    } else {
      throw Exception(response.statusMessage);
    }
  }
}
