import 'package:covid19/helpers.dart';
import 'package:covid19/models/index.dart';
import 'package:covid19/widgets/index.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:covid19/extensions.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DetailPage extends StatelessWidget {
  static final String route = '/detail';

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> args = ModalRoute.of(context).settings.arguments;
    final CountryModel country = args['country'];
    final DateTime latestDate = args['latestDate'];

    return Scaffold(
      appBar: AppBar(
        title: AppBarTitle(
          title: country.name,
          subtitle: 'As of ${latestDate.format([dd, ' ', MM, ' ', yyyy])}',
        ),
      ),
      body: Scrollbar(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverToBoxAdapter(
                child: SummaryCard(
                  title: 'Confirmed',
                  badgeColor: Colors.blue,
                  badgeDirection: country.confirmed.direction,
                  diff: country.confirmed.diff,
                  total: country.confirmed.total,
                  today: country.confirmed.today,
                ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0).copyWith(top: 0.0),
              sliver: SliverToBoxAdapter(
                child: SummaryCard(
                  title: 'Deaths',
                  badgeColor: Colors.red,
                  badgeDirection: country.deaths.direction,
                  diff: country.deaths.diff,
                  total: country.deaths.total,
                  today: country.deaths.today,
                ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0).copyWith(top: 0.0),
              sliver: SliverToBoxAdapter(
                child: SummaryCard(
                  title: 'Recovered',
                  badgeColor: Colors.green,
                  badgeDirection: country.recovered.direction,
                  diff: country.recovered.diff,
                  total: country.recovered.total,
                  today: country.recovered.today,
                ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0).copyWith(top: 0.0),
              sliver: SliverToBoxAdapter(
                child: SummaryChart(
                  timeline: country.timeline,
                ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0).copyWith(top: 0.0),
              sliver: SliverToBoxAdapter(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Card(
                        margin: EdgeInsets.zero,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                '${Helpers.formatCurrency(country.recoveryRate, decimalDigits: 2)}%',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline
                                    .copyWith(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                              SizedBox(height: 16.0),
                              Text('Recovery Rate')
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16.0),
                    Expanded(
                      child: Card(
                        margin: EdgeInsets.zero,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                '${Helpers.formatCurrency(country.deathRate, decimalDigits: 2)}%',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline
                                    .copyWith(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                              SizedBox(height: 16.0),
                              Text('Death Rate')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
