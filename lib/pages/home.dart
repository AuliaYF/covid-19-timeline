import 'package:covid19/blocs/dashboard/dashboard_bloc.dart';
import 'package:covid19/repos/index.dart';
import 'package:covid19/screens/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  static final String route = '/';

  @override
  Widget build(BuildContext context) {
    final GeneralRepository repo = Provider.of<GeneralRepository>(context);

    return BlocProvider<DashboardBloc>(
      create: (context) => DashboardBloc(repo: repo),
      child: ChangeNotifierProvider<SortOptionProvider>(
        create: (context) => SortOptionProvider(),
        child: Scaffold(
          body: HomeScreen(),
        ),
      ),
    );
  }
}

class SortOptionProvider with ChangeNotifier {
  SortOptions _sort = SortOptions.countryAsc;

  set sort(SortOptions sort) {
    this._sort = sort;

    notifyListeners();
  }

  SortOptions get sort => this._sort;
}
