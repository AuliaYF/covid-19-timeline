import 'package:date_format/date_format.dart';

extension DateFormatExtension on DateTime {
  String format(List<String> formats) {
    return formatDate(this, formats);
  }
}
